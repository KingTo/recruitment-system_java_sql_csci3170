import java.sql.*;
import java.util.Scanner;
import java.util.Date;

public class Employer {

    static Connection con = Main.con;

    Employer() {
       
    }

    public int checkEmployer(String id) {
        int value = 0;
        try {

            //insert the query
            String checkemployersql = "SELECT COUNT(1) AS employer_id " +
                                      "FROM Employer " +
                                     "WHERE employer_id = ?";
            PreparedStatement pstmt = con.prepareStatement(checkemployersql);
            //pstmt.setInt(1, Integer.parseInt(id));
            pstmt.setString(1, id);
            //debug//System.out.println(pstmt+"\n\n\n");

            //get the result
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            //debug//System.out.println(rs+"\n\n\n");
            value = rs.getInt("employer_id");

        } catch(SQLException se) {
            System.out.println("[ERROR] Check Employer failed.\n");
            se.printStackTrace();
        }
          //debug System.out.println("Print"+ value);
        return value;
    }

    public int checkWorkingOrNot(String employee_id){
          try {
            String checkWorkingOrNotsql = "SELECT COUNT(*) AS counter " +
                          "FROM EmploymentHistory H " +
                          "WHERE " +
                          "H.employee_id = ? AND " +
                          "H.end = 0000000000 " ;
            PreparedStatement pstmt = con.prepareStatement(checkWorkingOrNotsql);
            pstmt.setString(1, employee_id);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            //debug
            //System.out.println("The counter ="+rs.getInt("counter"));
            if(rs.getInt("counter")==1){
                //debug//System.out.println("Working in company");
                return 1;
            }

          } catch(SQLException se) {
            System.out.println("[ERROR] Find Working Time failed.\n");
            se.printStackTrace();
          }
          return 0;

    }

    public int createNewPosition(String employer_id, String position_title, int salary, int experience){
        try {


            String createNewPositionsql = "INSERT INTO Position1" +
                                        " (position_id, position_title, salary, experience, employer_id, status)" +
                                        "VALUES (?, ?, ?, ?, ?, ?)";

            PreparedStatement pstmt = con.prepareStatement(createNewPositionsql);
            String temp = employer_id.substring(0, Math.min(employer_id.length(), 3));
            temp += position_title.substring(0, Math.min(position_title.length(), 3));
            //System.out.println(temp);
            pstmt.setString(1, temp);
            pstmt.setString(2, position_title);
            pstmt.setInt(3, salary);
            pstmt.setInt(4, experience);
            pstmt.setString(5, employer_id);
            pstmt.setBoolean(6, true);
            pstmt.executeUpdate();
            //debug  System.out.println("Your position is posted.");

        } catch(SQLException se) {
            se.printStackTrace();
            System.out.println("[ERROR] Create Position failed.\n");
        }
        return 1;       
    }   

    public void checkPotentialEmployees(String employer_id, String position_title, int salary, int experience){

        try {
                String checkPotentialEmployeessql = "SELECT * " + 
                                                    "FROM Employee E "+
                                                    "WHERE " +
                                                    "E.expected_salary <= ? AND " +
                                                    "E.experience >= ?";

            PreparedStatement pstmt = con.prepareStatement(checkPotentialEmployeessql);
            
            pstmt.setInt(1, salary);
            pstmt.setInt(2, experience);
            //debug//System.out.println(pstmt);          
            ResultSet rs = pstmt.executeQuery();

            int counter = 0;
            //debug//int temp =0;
            while(rs.next()){
                //debug temp += 1;
                //check Working Or Not
                //System.out.println(rs.getString("E.employee_id") +" ");
                if(checkWorkingOrNot(rs.getString("E.employee_id"))==1){
                    //debug//
                    //System.out.println(rs.getString("E.employee_id")+ " Working in company");
                    continue;
                }

                String jobList = rs.getString("E.skills");
                //debug//System.out.println(jobList+"\n\n\n");
                String[] parts = jobList.split(";");

                for(int i=0; i< parts.length; i++){
                  if(position_title.equals(parts[i])){
                    //System.out.println(rs.getString("E.employee_id")+ "  Not Working in company");
                    counter += 1;
                    break;
                  }   
                  /*
                    //debug
                    System.out.print(rs.getString("E.employee_id") +" ");
                    System.out.println(parts[i]);
                    System.out.println();
                    //*/
                    //System.out.println(rs.getString("E.employee_id")+ "  Not Working in company");
                } 
            }
            //debug//System.out.println("Total record =" + temp);
            if(counter>=1){
                int temp = 0;
                temp = createNewPosition(employer_id, position_title, salary, experience); 
                if(temp==1){
                    System.out.println(counter + " potential employees are found. The position recruitment is posted.");
                }
            }
            if(counter==0){
                System.out.println("No potntial employee are found");    
            }


        } catch(SQLException se) {
            System.out.println("[ERROR] Post postition recruitment failed.\n");
            se.printStackTrace();
        }
    }


    public void postPositionRecruitment() {
        System.out.println("Please enter your ID.");

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        String id = reader.nextLine();

        int employerValid = checkEmployer(id);
        if (employerValid == 0) {
            System.out.println("[ERROR] Employer not found.\n");
            return;
        }

        System.out.println("Please enter the position title.");

        Scanner reader2 = new Scanner(System.in);  // Reading from System.in
        String title = reader2.nextLine();

        System.out.println("Please an upper bound of salary.");

        Scanner reader3 = new Scanner(System.in);  // Reading from System.in
        int salary = reader3.nextInt();

        System.out.println("Please enter the requried experience(please enter to skip).");

        Scanner reader4 = new Scanner(System.in);  // Reading from System.in
        int experience = reader4.nextInt();
        //if (experience ==""){
          //  experience = 0;
        //}
        //debug System.out.println(id + title + salary + experience);
        checkPotentialEmployees(id, title, salary, experience);
        /*//debug//
        if (employerValid == 1) {
            System.out.println("Exists");
            return;
        }
        */
    }
    //------------------------------------------------------------------------------------------------------
    public void hiringEmployee(){

        System.out.println("Please enter your ID.");
        Scanner reader1 = new Scanner(System.in);  // Reading from System.in
        String employer_id = reader1.nextLine();
        
        int employerValid = checkEmployer(employer_id);
        if (employerValid == 0) {
            System.out.println("[ERROR] Employer not found.\n");
            return;
        }

        System.out.println("Please enter the employee_id that you want to hire.");
        Scanner reader2 = new Scanner(System.in);  // Reading from System.in
        String employee_id = reader2.nextLine();

        //find the position and company and interview
        String temp = valueFromMarkInterestedPosition(employee_id, employer_id);

        if(temp.equals("Nothing")){
            System.out.println("Not a suitable employer");
            return;
        }
        
        //1: position_id, 2: interview, 3: company
        String[] parts = temp.split(",");
            /*
            for(int i=0; i< parts.length; i++){         
                    System.out.println(parts[i]);
            }
            */

        if(Boolean.parseBoolean(parts[1])==true){
        //System.out.println("Interview is True");
            String position_id = parts[0];
            String company = parts[2];
        //For hiring the employee
            updatePositionRecord(employer_id, position_id);
            deleteRecordInMarkedPosition(employee_id, position_id);
            createNewHistory(employee_id, position_id, company);
        }
    }

    public String valueFromMarkInterestedPosition(String employee_id, String employer_id){
        String temp="Nothing";
        try{
            String valueFromMarkInterestedPositionsql =  "SELECT * " +
                                                "FROM MarkInterestedPosition M, Employer R " +
                                                "WHERE " +
                                                "M.employee_id = ? AND " +
                                                "R.employer_id = ? " +
                                                "GROUP BY M.employee_id ";
            PreparedStatement pstmt = con.prepareStatement(valueFromMarkInterestedPositionsql);
            pstmt.setString(1, employee_id);  
            pstmt.setString(2, employer_id);  
            //System.out.println(pstmt);
            ResultSet rs = pstmt.executeQuery();


            if(rs.next()){
            /*
            System.out.println( rs.getString("M.position_id") + ", " +
                                rs.getBoolean("M.interview") + ", " +
                                rs.getString("R.company"));
            */
                temp = rs.getString("M.position_id") + "," ;
                temp += rs.getBoolean("M.interview") + "," ;
                temp += rs.getString("R.company");
                return temp;
            }                             
        }
        catch(SQLException se) {
            System.out.println("[ERROR] Cannot retrive the data from Mark Interested Position record.\n");
            se.printStackTrace();
        }
        return temp;
    }

    public void checkEmployeesAndInterview(){
        System.out.println("Please enter your ID.");

        Scanner reader1 = new Scanner(System.in);  // Reading from System.in
        String employer_id = reader1.nextLine();

        int employerValid = checkEmployer(employer_id);
        if (employerValid == 0) {
            System.out.println("[ERROR] Employer not found.\n");
            return;
        }

        checkPositionPostByEmployer(employer_id);

        System.out.println("Please pick one position id.");
        Scanner reader2 = new Scanner(System.in);  // Reading from System.in
        String position_id = reader2.nextLine();

        int temp1 =findInterestedEmployee(position_id);
        if(temp1==0){
            System.out.println("No employees who mark interested in this postition recruitment");
            return;
        }

        System.out.println("Please pick one employee by Employer_id.");
        Scanner reader3 = new Scanner(System.in);  // Reading from System.in
        String employee_id = reader3.nextLine();

        //arrange interview
        arrangeInterview(employee_id, position_id);

        System.out.println("An IMMEDIATE interview has done.");   
    }

    public void arrangeInterview(String employee_id, String position_id){
        try{
            String updateRecordsql =    "Update MarkInterestedPosition " +
                                        "SET interview = true " +
                                        "WHERE " +
                                        "position_id=? AND " +
                                        "employee_id=? " ;
            PreparedStatement pstmt = con.prepareStatement(updateRecordsql);
            pstmt.setString(1, position_id);  
            pstmt.setString(2, employee_id);  
            //System.out.println(pstmt);
            pstmt.executeUpdate();
                                    
        }
        catch(SQLException se) {
            System.out.println("[ERROR] Update Mark Interested Position record failed.\n");
            se.printStackTrace();
        }
    }

    //for MarkInterestedPosition
    public void deleteRecordInMarkedPosition(String employee_id, String position_id){
        try{
            String findInterestedEmployeesql =  "DELETE " +
                                                "FROM MarkInterestedPosition " +
                                                "WHERE " +
                                                "position_id = ? AND " +
                                                "employee_id = ? " ;
            PreparedStatement pstmt = con.prepareStatement(findInterestedEmployeesql);
            pstmt.setString(1, position_id);  
            pstmt.setString(2, employee_id);  
            //System.out.println(pstmt);
            pstmt.executeUpdate();
                                    
        }
        catch(SQLException se) {
            System.out.println("[ERROR] Delete Record failed.\n");
            se.printStackTrace();
        }
    }

    //for EmploymentHistory
    public int createNewHistory(String employee_id, String position_id, String company){
        try {


            String createNewPositionsql = "INSERT INTO EmploymentHistory" +
                                        " (employee_id, company, position_id, start)" +
                                        "VALUES (?, ?, ?, ?)";

            PreparedStatement pstmt = con.prepareStatement(createNewPositionsql);

            //System.out.println(temp);
            pstmt.setString(1, employee_id);
            pstmt.setString(2, company);//
            pstmt.setString(3, position_id);
            Date date= new Date();
            Timestamp current = new Timestamp(date.getTime());
            pstmt.setTimestamp(4, current);//
            //pstmt.setTimestamp(5, (current-current));//
            pstmt.executeUpdate();
            //debug  System.out.println("Your position is posted.");
            System.out.println("An Employment History record is created, details are:");
            System.out.println("Employee_Id, Company, Position_ID, Start, End");
            System.out.println(employee_id + ", " + company + ", " + position_id + ", " + current + ", " + "NULL");

        } catch(SQLException se) {
            se.printStackTrace();
            System.out.println("[ERROR] Create Position failed.\n");
        }
        return 1;       
    }

    //for Position1
    public void updatePositionRecord(String employer_id, String position_id){
        try{
            String updateRecordsql =    "Update Position1 " +
                                        "SET status = 0 " +
                                        "WHERE " +
                                        "position_id=? AND " +
                                        "employer_id=? " ;
            PreparedStatement pstmt = con.prepareStatement(updateRecordsql);
            pstmt.setString(1, position_id);  
            pstmt.setString(2, employer_id);  
            //System.out.println(pstmt);
            pstmt.executeUpdate();
                                    
        }
        catch(SQLException se) {
            System.out.println("[ERROR] Update  Position Record failed.\n");
            se.printStackTrace();
        }
    }

    public int findInterestedEmployee(String position_id){
        int count = 0;
        try{
            String findInterestedEmployeesql =  "SELECT * " +
                                                "FROM MarkInterestedPosition M " +
                                                "WHERE M.position_id = ? " +
                                                "GROUP BY M.employee_id ";
            PreparedStatement pstmt = con.prepareStatement(findInterestedEmployeesql);
            pstmt.setString(1, position_id);  
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                count += 1;
                if(count==1){
                    System.out.println("The employees who mark interested in this postition recruitment are");
                    System.out.println("Employer_Id, Name, Expected_Salary, Experience, Skills");
                }
                System.out.println(rs.getString("M.employee_id") + ", " +
                                   rs.getString("M.name") + ", " +
                                   rs.getInt("M.expected_salary") + ", " +
                                   rs.getInt("M.experience") + ", " +
                                   rs.getString("M.skills"));
            } 
                                    
        }
        catch(SQLException se) {
            System.out.println("[ERROR] Find Interested Employee failed.\n");
            se.printStackTrace();
        }
        return count;
    }

    public void checkPositionPostByEmployer(String employer_id){
        try {
            String checkEmployerPositionRecruitmentsql = "SELECT P.position_id " +
                                                         "FROM Position1 P " +
                                                         "WHERE " +
                                                         "P.employer_id = ?";
            PreparedStatement pstmt = con.prepareStatement(checkEmployerPositionRecruitmentsql);
            pstmt.setString(1, employer_id); 
            //debug//System.out.println(pstmt);          
            ResultSet rs = pstmt.executeQuery();

            System.out.println("The id of position recruitments posted by you are:");
            while (rs.next()) {
                System.out.println(rs.getString("P.position_id")); 
            } 

        }

        catch(SQLException se) {
            System.out.println("[ERROR] Check Position exist failed.\n");
            se.printStackTrace();
        }
    }


    //------------------------------------------------------------------------------------------------------
    public void operation() {
        while (true) {
            System.out.println("\nEmployer, what would you like to do?");

            System.out.println("1. Post Position Requirement");
            System.out.println("2. Check employees and arrange an interview");
            System.out.println("3. Accept an employee");
            System.out.println("4. Go back");

            Scanner reader = new Scanner(System.in);  // Reading from System.in
            int n = reader.nextInt();
            int option = n;

            switch(option) {
                case 1: 
                        postPositionRecruitment();
                        break;
                case 2: 
                        checkEmployeesAndInterview();
                        break;
                case 3: 
                        hiringEmployee();
                        break;
                case 4: 
                        System.out.println();
                        return;
            }
        }
    }
}

