import java.sql.*;
import java.util.Scanner;

public class Administrator {

    static Connection con = Main.con;

    Administrator() {
       
    }


    public void createTables() {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            String employeeSql = "Create table Employee" +
                                 "(employee_id CHAR(6) NOT NULL," +
                                 "name VARCHAR(30) NOT NULL," +
                                 "expected_salary INTEGER," +
                                 "experience INTEGER," +
                                 "skills VARCHAR(50) NOT NULL," +
                                 "PRIMARY KEY(employee_id))" ;
                                // "PRIMARY KEY(employee_id)," +
                                // "CHECK (expected_salary > 0)," +
                                // "CHECK (experience > 0)";

            String companySql = "Create table Company" +
                               "(company CHAR(30) NOT NULL," +
                               "size INTEGER," +
                               "founded INTEGER," +
                               "PRIMARY KEY(company))" ;
                              // "PRIMARY KEY(company)," +
                               //"CHECK (size > 0)," +
                               //"CHECK (founded > 0)";


            String employerSql = "Create table Employer" +
                                  "(employer_id CHAR(6) NOT NULL," +
                                  "name VARCHAR(30) NOT NULL," +
                                  "company VARCHAR(30) NOT NULL," +
                                  "PRIMARY KEY(employer_id)," +
                                  "FOREIGN KEY(company) REFERENCES Company(company) ON DELETE CASCADE)";
                
            String positionSql = "Create table Position1" +
                                "(position_id CHAR(6) NOT NULL," +
                                "position_title VARCHAR(30) NOT NULL," +
                                "salary INTEGER," +
                                "experience INTEGER," +
                                "employer_id VARCHAR(6) NOT NULL," +
                                "status BOOLEAN," +
                                "PRIMARY KEY(position_id)," +
                                "FOREIGN KEY(employer_id) REFERENCES Employer(employer_id) ON DELETE CASCADE)";

            String employmentHistorySql  = "Create table EmploymentHistory" +
                              "(employee_id CHAR(6) NOT NULL," +
                              "company VARCHAR(30) NOT NULL," +
                              "position_id CHAR(6) NOT NULL," +
                              "start TIMESTAMP," +
                              "end TIMESTAMP NULL DEFAULT NULL," +
                              "FOREIGN KEY(employee_id) REFERENCES Employee(employee_id) ON DELETE CASCADE," +
                              "FOREIGN KEY(company) REFERENCES Company(company) ON DELETE CASCADE," + 
                              "FOREIGN KEY(position_id) REFERENCES Position1(position_id) ON DELETE CASCADE)";

            String markInterestedPositionSql  = "Create table MarkInterestedPosition" +
                              "(employee_id CHAR(6) NOT NULL," +
                              "name VARCHAR(30) NOT NULL," +
                              "expected_salary INTEGER," +
                              "experience INTEGER," +
                              "skills VARCHAR(50) NOT NULL," +
                              "position_id CHAR(6) NOT NULL," + 
                              "interview BOOLEAN," + 
                              "FOREIGN KEY(employee_id) REFERENCES Employee(employee_id) ON DELETE CASCADE," +
                              "FOREIGN KEY(position_id) REFERENCES Position1(position_id) ON DELETE CASCADE)";


            stmt.executeUpdate(employeeSql);
            stmt.executeUpdate(companySql);
            stmt.executeUpdate(employerSql);
            stmt.executeUpdate(positionSql);
            stmt.executeUpdate(employmentHistorySql);
            stmt.executeUpdate(markInterestedPositionSql);

            System.out.println("Processing...Done! Tables are created!\n");

        } catch(SQLException se) {
            se.printStackTrace();
            System.out.println("[Error]: All tables have already existed. Create table failed. Try to delete all tables first\n");
        }
    }

    public void deleteTables() {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            String delEmployeeSql = "DROP TABLE IF EXISTS Employee";
            String delCompanySql = "DROP TABLE IF EXISTS Company";
            String delEmployerSql = "DROP TABLE IF EXISTS Employer";
            String delPositionSql = "DROP TABLE IF EXISTS Position1";
            String delEmploymentHistorySql = "DROP TABLE IF EXISTS EmploymentHistory";
            String delMarkInterestedPositionSql = "DROP TABLE IF EXISTS MarkInterestedPosition";

            stmt.executeUpdate(delMarkInterestedPositionSql);
            stmt.executeUpdate(delEmploymentHistorySql);
            stmt.executeUpdate(delPositionSql);           
            stmt.executeUpdate(delEmployerSql);
            stmt.executeUpdate(delCompanySql);
            stmt.executeUpdate(delEmployeeSql);

            System.out.println("Processing...Done! Tables are deleted!\n");

        } catch(SQLException se) {
            System.out.println("[Error]: Delete tables error.\n");
            se.printStackTrace();
        }
    }

    public void loadData() {
        Scanner keyboard;
        Statement stmt = null;
        try {
            stmt = con.createStatement();


            System.out.println("Please enter the folder path.");
            Scanner reader = new Scanner(System.in);  // Reading from System.in
            String path = reader.nextLine();


            String employeePath = path + "/employee.csv";
            String companyPath = path + "/company.csv";
            String employerPath = path + "/employer.csv";
            String positionPath = path + "/position.csv";
            String employmentHistoryPath = path + "/history.csv";

            String loadEmployeeSql = "LOAD DATA LOCAL INFILE \'" + employeePath + "\'" +
                                    " INTO TABLE Employee" +
                                    " FIELDS TERMINATED BY \',\'" +
                                    " LINES TERMINATED BY \'\\n\'" +
                                    " (employee_id, name, expected_salary, experience, skills)";

            String loadCompanySql = "LOAD DATA LOCAL INFILE \'" + companyPath + "\'" +
                                   " INTO TABLE Company" +
                                   " FIELDS TERMINATED BY \',\'" +
                                   " LINES TERMINATED BY \'\\n\'" +
                                   " (company, size, founded)";

            String loadEmployerSql = "LOAD DATA LOCAL INFILE \'" + employerPath + "\'" +
                                      " INTO TABLE Employer" +
                                      " FIELDS TERMINATED BY \',\'" +
                                      " LINES TERMINATED BY \'\\n\'" +
                                      " (employer_id, name, company)";

            String loadPositionSql = "LOAD DATA LOCAL INFILE \'" + positionPath + "\'" +
                                      " INTO TABLE Position1" +
                                      " FIELDS TERMINATED BY \',\'" +
                                      " LINES TERMINATED BY \'\\n\'" +
                                      " (position_id, position_title, salary, experience, employer_id, @status)" +
                                      "SET status = (@status = 'TRUE')";

            String loadEmploymentHistorySql = "LOAD DATA LOCAL INFILE \'" + employmentHistoryPath + "\'" +
                                      " INTO TABLE EmploymentHistory" +
                                      " FIELDS TERMINATED BY \',\'" +
                                      " LINES TERMINATED BY \'\\n\'" +
                                      " (employee_id, company, position_id, start, end)";
            //debug//System.out.println(loadEmployeeSql);
            //debug//System.out.println('/n/n/n');
            stmt.executeUpdate(loadEmployeeSql);
            stmt.executeUpdate(loadCompanySql);
            stmt.executeUpdate(loadEmployerSql);
            stmt.executeUpdate(loadPositionSql);
            stmt.executeUpdate(loadEmploymentHistorySql);

            System.out.println("Processing...Data is loaded!\n");

        } catch(SQLException se) {
            se.printStackTrace();
            System.out.println("[Error]: Load data error.\n");
        }
    }

    public void checkData() {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            String checkEmployee = "SELECT COUNT(*) AS total FROM Employee";
            String checkCompany = "SELECT COUNT(*) AS total FROM Company";
            String checkEmployer = "SELECT COUNT(*) AS total FROM Employer";
            String checkPosition = "SELECT COUNT(*) AS total FROM Position1";
            String checkEmploymentHistory = "SELECT COUNT(*) AS total FROM EmploymentHistory";

            ResultSet rs = stmt.executeQuery(checkEmployee);
            rs.next();
            System.out.println("Number of records in each table:");
            System.out.println("Table Employee: " + rs.getInt("total"));

            rs = stmt.executeQuery(checkCompany);
            rs.next();
            System.out.println("Table Company: " + rs.getInt("total"));

            rs = stmt.executeQuery(checkEmployer);
            rs.next();
            System.out.println("Table Employer: " + rs.getInt("total"));

            rs = stmt.executeQuery(checkPosition);
            rs.next();
            System.out.println("Table Position: " + rs.getInt("total"));

            rs = stmt.executeQuery(checkEmploymentHistory);
            rs.next();
            System.out.println("Table EmploymentHistory: " + rs.getInt("total") + "\n");

        } catch(SQLException se) {
            System.out.println("[Error]: Check data error.\n");
            se.printStackTrace();
        }

    }

    public void operation() {
        while (true) {
            System.out.println("\nAdministrator, what would you like to do?");

            System.out.println("1. Create tables");
            System.out.println("2. Delete tables");
            System.out.println("3. Load data");
            System.out.println("4. Check data");
            System.out.println("5. Go back");

            Scanner reader = new Scanner(System.in);  // Reading from System.in
            int n = reader.nextInt();
            int option = n;

            switch(option) {
                case 1: createTables();
                        break;
                case 2: deleteTables();
                        break;
                case 3: loadData();
                        break;
                case 4: checkData();
                        break;
                case 5: System.out.println();
                        return;
            }
        }
    }
}
