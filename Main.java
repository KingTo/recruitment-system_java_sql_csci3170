import java.sql.*;
import java.util.Scanner;

public class Main {
    static String dbAddress = "jdbc:mysql://projgw.cse.cuhk.edu.hk:2633/db20";
    static String dbUsername = "Group20";
    static String dbPassword = "CSCI3170";
    static Connection con = null;

    public static void main(String[] args) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(dbAddress, dbUsername, dbPassword);
        } catch (ClassNotFoundException e) {
            System.out.println("[ERROR] Java MySQL DB Driver not found.");
            System.exit(0);
        } catch (SQLException se) {
            System.out.println("[ERROR] SQL Exception in JDBC Connection");
            System.exit(0);
        }

        System.out.println("Connection Success\n\n");

        Administrator admins = new Administrator();
        Employee employee = new Employee();
        Employer employer = new Employer();
        loop: while (true) {

            System.out.println("Welcome! Who are you?");
            System.out.println("1. An adminstrator");
            System.out.println("2. An employee");
            System.out.println("3. An employer");
            System.out.println("4. Exit");
            System.out.println("Please enter [1-4]");

            Scanner reader = new Scanner(System.in);  // Reading from System.in
            int n = reader.nextInt();
            int option = n;
            //reader.close();
            //System.out.println(n);


            switch(option) {
                case 1: 
                        //System.out.println("1.");
                        admins.operation();
                        break;
                case 2: 
                        employee.operation();
                        System.out.println("2.");
                        break;
                case 3: 
                        employer.operation();
                        System.out.println("3.");
                        break;
                case 4: 
                        System.out.println("GoodBye~~");
                        break loop;
            }
        }

        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println("[ERROR] SQL Exception happens when closing JDBC Connection");
        }
        
    }
}
