import java.sql.*;
import java.util.Scanner;
import java.text.*;

public class Employee {

    static Connection con = Main.con;

    Employee() {
       
    }

    public int checkEmployee(String id) {
        int value = 0;
        try {

            //insert the query
            String checkemployeesql = "SELECT COUNT(1) AS employee_id " +
                                      "FROM Employee " +
                                     "WHERE employee_id = ?";
            PreparedStatement pstmt = con.prepareStatement(checkemployeesql);
            //pstmt.setInt(1, Integer.parseInt(id));
            pstmt.setString(1, id);
            //debug//System.out.println(pstmt+"\n\n\n");

            //get the result
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            //debug//System.out.println(rs+"\n\n\n");
            value = rs.getInt("employee_id");

        } catch(SQLException se) {
            System.out.println("[ERROR] Check Employee failed.\n");
            se.printStackTrace();
        }
          //debug System.out.println("Print"+ value);
        return value;
    }   

    public void checkPositionExist(String employee_id, int functionChoice) {
        try {
            String checkPositionExistsql = "SELECT P.position_id, P.position_title, P.salary, E.skills, C.company, C.size, C.Founded " +
                          "FROM Position1 P, Employee E, Employer R, Company C " +
                          "WHERE " +
                          "E.employee_id = ? AND " +
                          "P.status = ? AND " +
                          "E.expected_salary <= P.salary AND " +
                          "E.experience >= P.experience AND " +
                          "P.employer_id = R.employer_id AND " +
                          "C.company = R.company ";
            PreparedStatement pstmt = con.prepareStatement(checkPositionExistsql);

            pstmt.setString(1, employee_id);
            pstmt.setBoolean(2, true);  
            //debug//System.out.println(pstmt);          
            ResultSet rs = pstmt.executeQuery();

            if(functionChoice==1){
              System.out.println("\nYour available positions are:");
            }
            if(functionChoice==2){
              System.out.println("\nYour interested positions are:");
            }
            System.out.println("Position_ID, Postion_Title, Salary, Company, Size, Founded");

            while (rs.next()) {
              String jobList = rs.getString("E.skills");
              //debug//System.out.println(jobList+"\n\n\n");
              String[] parts = jobList.split(";");

              for(int i=0; i< parts.length; i++){
                //debug
                //System.out.println("current element is: " + parts[i]);
                //System.out.println(rs.getString("P.position_title"));
                //System.out.println(parts[i]);
                //System.out.println("\n");

                //for show position
                if(functionChoice==1){
                  if(rs.getString("P.position_title").equals(parts[i])){
                  //debug
                  //System.out.println(rs.getString("P.position_title"));
                  //System.out.println(parts[i]);
                    System.out.println(rs.getString("P.position_id") + " , " + 
                                   rs.getString("P.position_title") + " , " + 
                                   rs.getString("P.salary") + " , " + 
                                   rs.getString("C.company") + " , " +  
                                   rs.getString("C.size") + " , " + 
                                   rs.getString("C.Founded"));
                   //System.out.println();
                    break;
                  }
               }


                //for Mark interested position
                if(functionChoice==2){
                  if(rs.getString("P.position_title").equals(parts[i])){
                    if(checkHistoryCompanyExist(employee_id, rs.getString("C.company"))==0){
                  //debug
                  //System.out.println(rs.getString("P.position_title"));
                  //System.out.println(parts[i]);
                    System.out.println(rs.getString("P.position_id") + " , " + 
                                   rs.getString("P.position_title") + " , " + 
                                   rs.getString("P.salary") + " , " + 
                                   rs.getString("C.company") + " , " +  
                                   rs.getString("C.size") + " , " + 
                                   rs.getString("C.Founded"));
                   //System.out.println();
                    break;
                    }
                  }
                }
              }
                //debug
                //System.out.println(sameTitleSkill+ "\n");
                //System.out.println(rs.getString("P.position_title"));
                //System.out.println(parts[n]+ "\n");                         
            }  
            //debug//System.out.println("DLLMMMMMMM----------");
            if(functionChoice==2){
              markPosition(employee_id); 
            }
            
         //   value = rs.getInt("exist");
        } catch(SQLException se) {
            System.out.println("[ERROR] Check Position exist failed.\n");
            se.printStackTrace();
        }
        //System.out.println("hi.\n");
    }

      //find the company that employee have been work for
     public int checkHistoryCompanyExist(String employee_id, String company_name) {
        int sameCompany = 0;
          try {
            String checkHistoryCompanyExistsql = "SELECT H.company " +
                          "FROM EmploymentHistory H " +
                          "WHERE " +
                          "H.employee_id = ? " +
                          "GROUP BY H.company " +
                          "HAVING COUNT(*) >= 1 " ;
            PreparedStatement pstmt = con.prepareStatement(checkHistoryCompanyExistsql);

            pstmt.setString(1, employee_id);
            //pstmt.setBoolean(2, true);  
            //debug//System.out.println(pstmt);    
            //System.out.println(pstmt);      
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                  if( rs.getString("H.company").equals(company_name)){
                    sameCompany=1;
                    break;
                  }
                  //debug
                  //System.out.println(rs.getString("H.company") );
            } 

        } catch(SQLException se) {
            System.out.println("[ERROR] Check Position exist failed.\n");
            se.printStackTrace();
        }

        return sameCompany;
      }


    public void markPosition(String employee_id) {


        String[] employeeInfo= findEmployeeInfo(employee_id);
        
        /*
        //debug
        System.out.println("This is markPosition.");
        for(int i=0;i<5;i++){
          System.out.println(employeeInfo[i]);
        }
        */

        //Remark the user must enter the job id in the job list
        // but it allow user to input any job to the job list, 
        //i.e the job can be the one that I do want lmao.
        System.out.println("\nPlease enter one interested Position_ID.");

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        String postion_id = reader.nextLine();

        // take request

        try {

            String markPositionsql =  "INSERT INTO MarkInterestedPosition" +
                                      "(employee_id, name, expected_salary, experience, skills, position_id, interview)" +
                                      "VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pstmt = con.prepareStatement(markPositionsql);

            pstmt.setString(1, employee_id);
            pstmt.setString(2, employeeInfo[1]);
            pstmt.setInt(3, Integer.parseInt(employeeInfo[2]));
            pstmt.setInt(4, Integer.parseInt(employeeInfo[3]));
            pstmt.setString(5, employeeInfo[4]);
            pstmt.setString(6, postion_id);
            pstmt.setBoolean(7, false);
            pstmt.executeUpdate();

        System.out.println("Done.");
        
        } catch(SQLException se) {
            se.printStackTrace();
            System.out.println("[ERROR] Take request failed.\n");
        }

    }

    public String[] findEmployeeInfo(String id) {
        String[] value = new String[5];
        try {

            //insert the query
            String findEmployeeInfosql = "SELECT *" +
                                      "FROM Employee E " +
                                     "WHERE employee_id = ?";
            PreparedStatement pstmt = con.prepareStatement(findEmployeeInfosql);
            pstmt.setString(1, id);

            //get the result
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            //debug
            /*
            System.out.println( rs.getString("E.employee_id") + " , " + 
                                rs.getString("E.name") + " , " + 
                                rs.getString("E.expected_salary") + " , " + 
                                rs.getString("E.experience") + " , " +  
                                rs.getString("E.skills"));
                                */
            value[0]=rs.getString("E.employee_id");
            value[1]=rs.getString("E.name");
            value[2]=rs.getString("E.expected_salary");
            value[3]=rs.getString("E.experience");
            value[4]=rs.getString("E.skills");

            /*
            //debug
            for(int i=0;i<5;i++){
              System.out.println(value[i]);
            }
            */

        } catch(SQLException se) {
            System.out.println("[ERROR] Check Employee failed.\n");
            se.printStackTrace();
        }
          //debug System.out.println("Print"+ value);
        return value;
    }  



      public void showAvailablePositions(int functionChoice){
        System.out.println("Please enter your ID.");

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        String id = reader.nextLine();

        int employeeValid = checkEmployee(id);
        if (employeeValid == 0) {
            System.out.println("[ERROR] Employee not found.\n");
            return;
        }
        checkPositionExist(id,functionChoice);
        /*//debug//
        if (employeeValid == 1) {
            System.out.println("Exists");
            return;
        }
        */
      }

      public void averageTime() {
          System.out.println("Please enter your ID.");

          Scanner reader = new Scanner(System.in);  // Reading from System.in
          String id = reader.nextLine();

          int employeeValid = checkEmployee(id);
          if (employeeValid == 0) {
              System.out.println("[ERROR] Employee not found.\n");
              return;
         }
         int temp1 = findWorkingTimeCount(id);
         /*
         //debug
         if(temp1==1){
            System.out.println("The count >=3 ");
         }
         if(temp1==0){
            System.out.println("The count <3 ");
         }
         */
      }

      public int findWorkingTimeCount(String employee_id) {
          try {
            String findWorkingTimeCountsql = "SELECT COUNT(*) AS counter " +
                          "FROM EmploymentHistory H " +
                          "WHERE " +
                          "H.employee_id = ? AND " +
                          "H.end != 0000000000 " ;
                         // "GROUP BY H.employee_id ";// +
                          //"HAVING COUNT(*) >= 3 " ;
            PreparedStatement pstmt = con.prepareStatement(findWorkingTimeCountsql);
            pstmt.setString(1, employee_id);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            //debug
            //System.out.println("The counter ="+rs.getInt("counter"));
            if(rs.getInt("counter")>=3){
              findWorkingTime(employee_id);
              return 1;
            }
            if(rs.getInt("counter")<3){
              System.out.println("Less than 3 records");
              return 0;
            }
            //System.out.println(pstmt);
          
          } catch(SQLException se) {
            System.out.println("[ERROR] Find Working Time failed.\n");
            se.printStackTrace();
          }
       return 0;
      }

      public void findWorkingTime(String employee_id) {
          try {
            String findWorkingTimesql = "SELECT * " +
                          "FROM EmploymentHistory H " +
                          "WHERE " +
                          "H.employee_id = ? AND " +
                          "H.end != 0000000000 " ;
                         // "GROUP BY H.employee_id ";// +
                          //"HAVING COUNT(*) >= 3 " ;
            PreparedStatement pstmt = con.prepareStatement(findWorkingTimesql);
            pstmt.setString(1, employee_id);
            ResultSet rs = pstmt.executeQuery();

            //debug/ 
            //System.out.println(pstmt);
            int totalDay=0;
            while (rs.next()) {
                totalDay += (int)(Math.floor(rs.getTimestamp("H.end").getTime()-rs.getTimestamp("H.start").getTime())/1000.0/60.0/60.0/24.0) ;
                /*
                  //debug
                  System.out.println( rs.getString("H.employee_id") + " , " + 
                                rs.getString("H.company") + " , " + 
                                rs.getString("H.position_id") + " , " + 
                                rs.getTimestamp("H.start") + " , " +  
                                rs.getTimestamp("H.end"));    
                  System.out.print("Your average working time is: ");
                  System.out.print( (int)( Math.floor(rs.getTimestamp("H.end").getTime()-rs.getTimestamp("H.start").getTime())/1000.0/60.0/60.0/24.0  ) );
                  System.out.println(" days.");
                  */
            }
            System.out.print("Your average working time is: ");
            System.out.print(totalDay);
            System.out.println(" days.");

        } catch(SQLException se) {
            System.out.println("[ERROR] Find Working Time failed.\n");
            se.printStackTrace();
        }
      }



    public void operation() {
        while (true) {
            System.out.println("\nEmployee, what would you like to do?");

            System.out.println("1. Show Available Positions");
            System.out.println("2. Mark Interested Position");
            System.out.println("3. Check Average Working Time");
            System.out.println("4. Go back");

            Scanner reader = new Scanner(System.in);  // Reading from System.in
            int n = reader.nextInt();
            int option = n;

            switch(option) {
                case 1: 
                        //checkEmployee("eebmri");
                        //checkPositionExist("eebmri");
                        //break;
                        showAvailablePositions(option);
                        break;
                case 2:  
                        //checkPositionExist("eebmri");
                        showAvailablePositions(option);
                        //checkHistoryCompanyExist("eebmri");

                        break;
                case 3: 

                        averageTime();
                        break;
                case 4: 
                        System.out.println();
                        return;
            }
        }
    }

}
