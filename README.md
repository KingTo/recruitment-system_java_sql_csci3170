# Recruitment System_JAVA_SQL_CSCI3170
> Usinh Java + JDBC API+ MySQL to creating Recruiment System

## Steps to compile and run the system

* Compile Ridesharing program `javac *.java`
* Running Ridesharing system `java -cp .:mysql-connector-java-5.1.47.jar Main` for linux
* Running Ridesharing system `java -cp .;mysql-connector-java-5.1.47.jar Main` for Win

## ER Diagram
> ![Image](ER_diagram.jpg)

## Relational Schema
> ![Image](relational_schema.JPG)

